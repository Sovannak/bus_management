package org.example;

import org.nocrala.tools.texttablefmt.BorderStyle;
import org.nocrala.tools.texttablefmt.CellStyle;
import org.nocrala.tools.texttablefmt.ShownBorders;
import org.nocrala.tools.texttablefmt.Table;

import java.util.Arrays;
import java.util.Objects;
import java.util.Scanner;

public class BusManagement {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String reset = "\u001B[0m", red = "\u001B[31m";
        int numberOfBus, numberOfSheet;
        System.out.println("---------- Setting up Bus ----------");
        System.out.print("Enter number of bus: ");
        numberOfBus = scanner.nextInt();
        System.out.print("Enter number sheet of bus: ");
        numberOfSheet = scanner.nextInt();
        System.out.println("------------- Bus Management System --------------");
        String[][] buses = new String[numberOfBus][numberOfSheet];
        for (String[] bus : buses) {
            Arrays.fill(bus, "(+)");
        }
        while (true){
            System.out.print(
                    """
                            1- Check Bus
                            2- Booking Bus
                            3- Cancel Booking
                            4- Reset Bus
                            5- Exit
                            """
            );
            System.out.print("Choose option (1-5): ");
            int option = scanner.nextInt();
            switch (option){
                case 1 -> {
                    System.out.println("---------- Display All Bus information ---------");
                    System.out.println("Id\t\tSheet\tAvailable\t\tUnavailable");
                    int available, unavailable, pageNo = 1, pageSize = 4, start, end, totalPage;
                    if(buses.length % pageSize == 0){
                        totalPage = buses.length / pageSize;
                    } else {
                        totalPage = (buses.length / pageSize) + 1;
                    }
                    start = (pageNo * pageSize) - pageSize;
                    if(pageNo == totalPage){
                        end = buses.length;
                    } else {
                        end = pageNo * pageSize;
                    }
                    CellStyle cellStyle = new CellStyle(CellStyle.HorizontalAlign.CENTER);
                    Table table = new Table(16, BorderStyle.UNICODE_BOX_HEAVY_BORDER_WIDE, ShownBorders.ALL);
                    table.addCell("Display All Bus Information", cellStyle, 16);
                    table.addCell("ID", cellStyle, 2);
                    table.addCell("Seat", cellStyle,4);
                    table.addCell("Available", cellStyle, 5);
                    table.addCell("Unavailable", cellStyle,5);
                    for (int i = start; i < end; i++){
                        available = 0;
                        unavailable = 0;
                        for (int j = 0; j < buses[i].length; j++){
                            if(Objects.equals(buses[i][j], "(+)")){
                                available++;
                            } else {
                                unavailable++;
                            }
                        }
                        table.addCell(String.valueOf((i + 1)), cellStyle, 2);
                        table.addCell(String.valueOf(numberOfSheet), cellStyle, 4);
                        table.addCell(String.valueOf(available), cellStyle, 5);
                        table.addCell(String.valueOf(unavailable), cellStyle, 5);
                    }
                    System.out.println(table.render());
                    System.out.print("Enter 0 to back or Bus Id to see detail: ");
                    int busId = scanner.nextInt();
                    if(busId == 0){
                        System.out.print("Back successful");
                    } else if (busId > buses.length) {
                        System.out.println("Sorry, we have only "+ numberOfBus +" buses");
                    } else {
                        System.out.println("---------- Display Bus "+ busId +" information ----------");
                        int availablePerBus = 0, unavailablePerBus = 0;
                        for (int i = 0; i < numberOfSheet; i++){
                            System.out.print(buses[busId - 1][i] + " " + (i + 1) + "\t\t");
                            if((i + 1) % 5 == 0){
                                System.out.println();
                            }
                            if(Objects.equals(buses[busId - 1][i], "(+)")){
                                availablePerBus++;
                            } else {
                                unavailablePerBus++;
                            }
                        }
                        System.out.println("\n( - ) : Unavailable("+ unavailablePerBus +") \t\t\t ( + ) : Available("+ availablePerBus +")");
                    }
                }
                case 2 -> {
                    System.out.print("Enter bus’s Id: ");
                    int busId = scanner.nextInt();
                    System.out.println("---------- Display Bus "+ busId +" information ----------");
                    int availablePerBus = 0, unavailablePerBus = 0;
                    for (int i = 0; i < numberOfSheet; i++){
                        System.out.print(buses[busId - 1][i] + " " + (i + 1) + "\t\t");
                        if((i + 1) % 5 == 0){
                            System.out.println();
                        }
                        if(Objects.equals(buses[busId - 1][i], "(+)")){
                            availablePerBus++;
                        } else {
                            unavailablePerBus++;
                        }
                    }
                    System.out.println("\n( - ) : Unavailable("+ unavailablePerBus +") \t\t\t ( + ) : Available("+ availablePerBus +")");
                    System.out.print("Enter Chair number to booking: ");
                    int chairNumber = scanner.nextInt();
                    System.out.print("Do you want to book chair number "+ chairNumber +"? (y/n): ");
                    String accept = scanner.next();
                    if(accept.equalsIgnoreCase("y")){
                        if (!Objects.equals(buses[busId - 1][chairNumber - 1], "(-)")){
                            buses[busId - 1][chairNumber - 1] = "(-)";
                            System.out.println("Chair number "+ chairNumber +" was booked successfully!");
                        } else {
                            System.out.println(red + "Chair number "+ chairNumber +" was booked already!" + reset);
                        }
                    }
                }
                case 3 -> {
                    System.out.print("Enter bus’s Id: ");
                    int busId = scanner.nextInt();
                    System.out.println("---------- Display Bus "+ busId +" information ----------");
                    int availablePerBus = 0, unavailablePerBus = 0;
                    for (int i = 0; i < numberOfSheet; i++){
                        System.out.print(buses[busId - 1][i] + " " + (i + 1) + "\t\t");
                        if((i + 1) % 5 == 0){
                            System.out.println();
                        }
                        if(Objects.equals(buses[busId - 1][i], "(+)")){
                            availablePerBus++;
                        } else {
                            unavailablePerBus++;
                        }
                    }
                    System.out.println("\n( - ) : Unavailable("+ unavailablePerBus +") \t\t\t ( + ) : Available("+ availablePerBus +")");
                    System.out.print("Enter sheet number to cancel booking: ");
                    int chairNumber = scanner.nextInt();
                    System.out.print("Do you want to cancel chair number "+ chairNumber +"? (y/n): ");
                    String accept = scanner.next();
                    if(accept.equalsIgnoreCase("y")){
                        buses[busId - 1][chairNumber - 1] = "(+)";
                        System.out.println("Chair number "+ chairNumber +" was canceled booking successfully!");
                    }
                }
                case 4 -> {
                    System.out.print("Enter bus’s Id: ");
                    int busId = scanner.nextInt();
                    System.out.print("Bus id "+ busId +" was reset with all seats available? (y/n): ");
                    String accept = scanner.next();
                    if(accept.equalsIgnoreCase("y")){
                        for (int i = 0; i < numberOfSheet; i++){
                            buses[busId - 1][i] = "(+)";
                        }
                        System.out.println("Bus id "+ busId +" was reset successfully");
                    }
                }
                case 5 -> {
                    System.out.println("Good bye!");
                    System.exit(0);
                }
            }
        }
    }
}